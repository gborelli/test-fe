interface AppConfig {
  apiUrl: string;
}

const config: AppConfig = {
  apiUrl: process.env.REACT_APP_APIURL || "",
};

export default config;
